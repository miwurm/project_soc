#-------------------------------------------------------------------------------
# file:      Makefile
# author:    Michael Wurm <wurm.michael95@gmail.com>
# copyright: 2019 Michael Wurm
# brief:     Makefile for Project SoC.
#-------------------------------------------------------------------------------


##--------------------------------------------------------------------
##Makefile Help for BlinkyLight.
##--------------------------------------------------------------------
##
##Supported Targets
##-----------------

bootloader: ##              - Generates preloader and bootloader.
	@bsp-generate-files --settings bsp/settings.bsp --bsp-dir ./bsp
	@cd ./bsp
	@make --directory=./bsp
	@make uboot --directory=./bsp
	@mkdir -p ./sd-boot-images
	@cp ./bsp/preloader-mkpimage.bin ./sd-boot-images
	@cp ./bsp/uboot-socfpga/u-boot.img ./sd-boot-images
	@echo "(MWURM) Sucessfully generated bootloader images to directory ./sd-boot-images"

clean: ##                   - Clean outputs.
	@echo "Cleaning..."
	git clean -x -d -f

help: ##                    - Show this help message.
	@grep -h "##" Makefile | grep -v "\"##\"" | sed -e 's/##//g'

.PHONY: bsp clean help

##
##--------------------------------------------------------------------
