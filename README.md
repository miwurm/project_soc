# Project SoC

Example project for the Terasic DE1-SoC development board.  

The purpose of this repository is to provide an environment to efficiently generate several files that are required for a SoC project.  
The respective files are the FPGA image and the embedded linux image, which already contains specific user space firmware.

### Features

- Embedded Linux
- Linux user space firmware
- FPGA Design

### How to run

All functionalities this repository supports, are executed by the Makefile.  
`make help` is your friend.

Make sure to check out submodules by running `git submodule update --init --recursive`.

### Contact
Michael Wurm <<wurm.michael95@gmail.com>>
